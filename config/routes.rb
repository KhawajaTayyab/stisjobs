StisJobs::Application.routes.draw do

  root to: "welcome_career/index", :to => "welcome_career#index"
  # jobs Posting Routes
  get "jobs/new", :to => "jobs#new", as: :jobs_new
  post "jobs/create"
  get "jobs/edit/:id", :to => "jobs#edit", as: :jobs_edit
  get "jobs/show/:id", :to => "jobs#show", as: :jobs_show
  get "jobs/index", :to => "jobs#index", as: :jobs_index
  delete "jobs/destroy/:id", :to => "jobs#destroy", as: :jobs_destroy
  post "jobs/update/:id", :to => "jobs#update", as: :jobs_update
  get "jobs/viewAllJobs", :to => "jobs#viewAllJobs", as: :jobs_viewAllJobs

  # Profile Routes
  get "profile/new", :to => "profile#new", as: :profile_new
  get "profile/edit/:id", :to => "profile#edit", as: :profile_edit
  get "profile/show/:id", :to => "profile#show", as: :profile_show
  get "profile/index", :to => "profile#index", as: :profile_index
  post "profile/createProfile"
  post "profile/update/:id", :to => "profile#update", as: :profile_update
  delete "profile/destroy/:id", :to => "profile#destroy", as: :profile_destroy
  post "profile/create"
  get "profile/download", :to => "profile#download"
  get "profile/apply/:id", :to => "profile#apply", as: :profile_apply

  get "attachments/new", :to => "attachments#new"
  post "attachments/create"
  get "attachments/show/:id", :to => "attachments#show", as: :attachments_show
  get "attachments/index", :to => "attachments#index"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
