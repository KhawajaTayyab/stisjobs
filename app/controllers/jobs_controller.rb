class JobsController < ApplicationController
	
  http_basic_authenticate_with name: "umer_rana@admin.com", password: "123456789", :only => [:index, :editJobs, :destroy]

  def new
  	@jobs = Jobs.new
  end

  def index
  	@jobs = Jobs.all
  end

  def show
  	@jobs = Jobs.find(params[:id])
  end

  def edit
  	@editJobs = Jobs.find(params[:id])
  end

  def create
  	@jobs = Jobs.new(jobs_params)
  	if @jobs.save
      flash[:notice] = "Job Successfully Post"
  		redirect_to jobs_index_path
  	else
  		render jobs_new_path
  	end
  end

  def update
  	@editJobs = Jobs.find(params[:id])
  	if @editJobs.update(params[:jobs].permit(:designation, :experience, :gender, :responsibilites, :skills, :qualification, :location,))
  		redirect_to jobs_index_path
  	else
  		render jobs_edit_path
  	end
  end

  def destroy
  	@jobs = Jobs.find(params[:id])
  	@jobs.delete

  	redirect_to jobs_index_path
  end

  def viewAllJobs
    @jobs = Jobs.all
  end

  private
  def jobs_params
  	params.require(:jobs).permit(:designation , :experience, :gender, :responsibilites, :skills, :qualification, :location,)
  end
end
