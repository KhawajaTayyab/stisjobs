class ProfileController < ApplicationController
	http_basic_authenticate_with name: "umer_rana@admin.com", password: "123456789", :only => [:index, :edit, :show, :destroy]

	def new
		#@jobs= Jobs.find(params[:id])
		@profile = Profile.new
	end

	def show
		@profile = Profile.find(params[:id])
	end

	def edit
		@editProfile = Profile.find(params[:id])
	end

	def index

		@profile = Profile.all
		@rel = ProfileAttachments.all
	end

	def update
		@editProfile = Profile.find(params[:id])
	  	if @editProfile.update(params[:profile].permit(:fullName, :email, :mobileNo, :phoneNo, :experience, :qualification, :certification, :jobDesignation))
	  		redirect_to profile_index_path
	  	else
	  		render profile_edit_path
	  	end
	end

	def create
		@profile = Profile.new(params_profile)
		#if !(@profile.errors.any?)
		if @profile.save
			profile = @profile.id
			@rel = ProfileAttachments.new(:profile_id => profile)
			@rel.save
			flash[:notice] = "You profile data successfully send. Kindly upload your cv"
			redirect_to attachments_new_path
		else
			flash[:alert] = "Email not be empty OR You will already applied through this email  Name Should not e blank"
			#flash[:notice] = "You have successfully logged out."
			redirect_to :back
		end
		# else
		# 	flash[:alert] = "Email not be empty"
		# 	redirect_to root_path	
		#end
		
	end

	def destroy
		@profile = Profile.find(params[:id])
		@profile.delete

		redirect_to profile_index_path
	end

	def apply
		@jobs = Jobs.find(params[:id])
		@profile = Profile.new
	end
	# def download
	# 	@profileID = params[:id]
	# 	rel = ProfileAttachments.where(:profile_id => @profileID)
	# 	result = rel.first.attachments_id
	# 	if !(result == nil)
	# 		redirect_to attachments_show_path(result)
	# 	end
	# end

	private
	def params_profile
		params.require(:profile).permit(:fullName, :email_id, :mobileNo, :phoneNo, :experience, :qualification, :certification, :jobDesignation)
	end
end
