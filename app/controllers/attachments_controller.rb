class AttachmentsController < ApplicationController
	http_basic_authenticate_with name: "umer_rana@admin.com", password: "123456789", :only => [:index, :show]

	def new
		@attachment = Attachments.new
	end

	def index
		@attachment = Attachments.all
		@profile = Profile.all	
	end

	def show
		@attachment = Attachments.find(params[:id])
		send_data(@attachment.data, :filename => @attachment.filename, :type => @attachment.content_type, disposition: "inline")
	end

	def create
		if not params[:attachment].blank?
			if ( (params[:attachment].content_type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") || ((params[:attachment].content_type == "application/pdf")))
		
				@attachment = Attachments.new
				
				@attachment.uploaded_file = params[:attachment]

				if @attachment.save
					profileId = Profile.last
					relProfileId = ProfileAttachments.where(:profile_id => profileId)
					attachmentId = @attachment.id
					relProfileId.first.attachments_id = attachmentId
					relProfileId.first.save
					flash[:alert] = "Thank You for applying"
					redirect_to root_path
				else
					flash[:alert] = "Problem in applying"
					redirect_to :back			
				end
			else
				flash[:alert] = "Allow pdf and docx file only"
				redirect_to :back	
			end

		else
			flash[:alert] = "File Not Present"
			redirect_to :back
		end

	end

end
