class Jobs < ActiveRecord::Base

	validates :designation, presence: true, length: {minimum: 2, maximum: 25}
	validates :responsibilites, presence: true, length: {minimum: 5, maximum: 250}
	validates :skills, presence: true, length: {minimum: 5, maximum: 250}
	validates :location, presence: true, length: {minimum: 2, maximum: 20}
end
