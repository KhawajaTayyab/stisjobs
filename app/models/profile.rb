class Profile < ActiveRecord::Base
    has_many :profile_attachments, :dependent => :restrict
    has_one :attachments, through: :profile_attachments

    validates :email_id, presence: true, uniqueness: true
end
