# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140130062318) do

  create_table "admins", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "firstName"
    t.string   "lastName"
    t.string   "userName"
    t.string   "country"
    t.string   "city"
    t.string   "street"
    t.string   "address"
    t.string   "contactNo"
    t.string   "postleCode"
    t.boolean  "approved",               default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true

  create_table "attachments", force: true do |t|
    t.string   "filename"
    t.string   "content_type"
    t.binary   "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "jobs", force: true do |t|
    t.string   "designation"
    t.string   "experience"
    t.string   "gender"
    t.string   "responsibilites"
    t.string   "skills"
    t.string   "qualification"
    t.string   "location"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "profile_attachments", force: true do |t|
    t.integer  "profile_id"
    t.integer  "attachments_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "attachmnetNo"
  end

  create_table "profiles", force: true do |t|
    t.string   "fullName"
    t.string   "mobileNo"
    t.string   "phoneNo"
    t.string   "experience"
    t.string   "qualification"
    t.string   "certification"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "jobDesignation"
    t.string   "email_id"
  end

  add_index "profiles", ["email_id"], name: "index_profiles_on_email_id", unique: true

end
