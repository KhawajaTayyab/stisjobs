class RemoveDataFormProfile < ActiveRecord::Migration
  def up
  	remove_column :profiles, :data
  	remove_column :profiles, :content_type
  	remove_column :profiles, :filename
  end
  def down
  	add_column :profiles, :data, :binary
  	add_column :profiles, :content_type, :string
  	add_column :profiles, :filename, :string
  end
end
