class AddJobDesginationToProfile < ActiveRecord::Migration
  def up
  	add_column :profiles, :jobDesignation, :string
  end
  def down
  	remove_column :profiles, :jobDesignation, :string
  end
end
