class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
    	t.string :fullName
    	t.string :email
    	t.string :mobileNo
    	t.string :phoneNo
    	t.string :experience
    	t.string :qualification
    	t.string :certification
    	t.column :filename, :string
    	t.column :content_type, :string
    	t.column :data, :binary
      t.timestamps
    end
  end
  def self.down
  	drop_table :profiles
  end
end
