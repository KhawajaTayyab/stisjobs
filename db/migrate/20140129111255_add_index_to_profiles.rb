class AddIndexToProfiles < ActiveRecord::Migration
  def up
    add_column :profiles, :email_id, :string
    add_index :profiles, :email_id, unique: true
  end
  def down
  	remove_column :profiles, :email_id, :string
    remove_index :profiles, :email_id, unique: true
  end
end
