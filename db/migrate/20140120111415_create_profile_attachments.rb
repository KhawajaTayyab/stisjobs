class CreateProfileAttachments < ActiveRecord::Migration
  def change
    create_table :profile_attachments do |t|
    	t.belongs_to :profile
    	t.belongs_to :attachments
      t.timestamps
    end
  end
end
