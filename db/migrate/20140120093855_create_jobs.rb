class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
    	t.string :designation
    	t.string :experience
    	t.string :gender
    	t.string :responsibilites
    	t.string :skills
    	t.string :qualification
    	t.string :location
      t.timestamps
    end
  end
end
